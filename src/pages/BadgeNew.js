import React from 'react';

import './styles/BadgeNew.css';
import header from '../images/platziconf-logo.svg';
import Badge from '../components/Badge';
import PageLoading from '../components/PageLoading';
import BadgeForm from '../components/BadgeForm';
import api from '../api';
import md5 from 'md5';
import sweetAlert from 'sweetalert2'

class BadgeNew extends React.Component {
  state = {
    loading: false,
    error: null,
    form: {
      firstName: '',
      lastName: '',
      email: '',
      jobTitle: '',
      twitter: '',
      avatarUrl:'',
    },
  };

alertError=()=>{
    sweetAlert.fire({
        title: 'Oops!',
        text: `Unexpected Error 😅, try again`,
        icon: 'error'
    });
}

alertData(data) {
    sweetAlert.fire({
        title: 'Stop!',
        text: `Fields to fill 🧐
          ${data}`,
        icon: 'error'
    });
}

alertSuccess() {
  sweetAlert.fire({
      title: 'Create Success !',
      text: 'We wait for you here 😊',
      icon: 'success'
  }).then((result) => {
      if (result.value || !result.value) {
          this.props.history.push('/badges');
      }
  });
}

handleChange = e => {
   let form = this.state.form;
   console.log(form)
  switch(e.target.name){
    case 'email':
      form = {
        ...this.state.form,
        [e.target.name]: e.target.value,
        avatarUrl: `https://www.gravatar.com/avatar/${md5(e.target.value)}?d=identicon&s=200`
      }
      break;
    default:
      form = {
        ...this.state.form,
        [e.target.name]: e.target.value,
      }
  }
  this.setState({
    form:form
  });
}

handleSubmit = async e => {
    e.preventDefault();
    const valuesFilter=Object.keys(this.state.form).filter((value)=>{
        return (this.state.form[value]==="")
    })
    if(valuesFilter.length!==0){
        this.alertData(valuesFilter)
    }else{
        this.setState({ loading: true, error: null });

        try {
          await api.badges.create(this.state.form);
          this.setState({ loading: false });
          this.alertSuccess()
        } catch (error) {
          this.setState({ loading: false, error: error });
          this.alertError()
        }
    }
};


  render() {
    if (this.state.loading) {
      return <PageLoading />;
    }
    return (
      <React.Fragment>
        <div className="BadgeNew__hero">
          <img
            className="BadgeNew__hero-image img-fluid"
            src={header}
            alt="Logo"
          />
        </div>

        <div className="container">
          <div className="row">
            <div className="col-6">
              <Badge
                firstName={this.state.form.firstName || 'FIRST_NAME'}
                lastName={this.state.form.lastName || 'LAST_NAME'}
                twitter={this.state.form.twitter || 'twitter'}
                jobTitle={this.state.form.jobTitle || 'JOB_TITLE'}
                email={this.state.form.email || 'EMAIL'}
                avatarUrl={this.state.form.avatarUrl || 'https://www.gravatar.com/avatar/21594ed15d68ade3965642162f8d2e84?d=identicon'}
                />
            </div>

            <div className="col-6">
              <h1>New Attendant</h1>
              <BadgeForm
                onChange={this.handleChange}
                onSubmit={this.handleSubmit}
                formValues={this.state.form}
              />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default BadgeNew;
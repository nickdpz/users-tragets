import React from 'react';
import PageLoading from '../components/PageLoading';
import PageError from '../components/PageError';
import BadgeDetails from '../pages/BadgeDetails';
import api from '../api';
import sweetAlert from 'sweetalert2';

class BadgeDetailsContainer extends React.Component {
    state = {
        loading: true,
        error: null,
        data: undefined,
        modalIsOpen: false,
      };

    alertError=()=>{
        sweetAlert.fire({
            title: 'Oops!',
            text: `Unexpected Error 😅, try again`,
            icon: 'error'
        });
    }
    
    alertSuccess() {
      sweetAlert.fire({
          title: 'Delete Success !',
          text: 'Coming Soon 😊',
          icon: 'success'
      }).then((result) => {
          if (result.value || !result.value) {
              this.props.history.push('/badges');
          }
      });
    }

    

    handleOpenModal = e => {
        this.setState({ modalIsOpen: true });
    };
    
    handleCloseModal = e => {
        this.setState({ modalIsOpen: false });
    };

    handleDeleteBadge = async e => {
        e.preventDefault();
        this.setState({ loading: true, error: null });
        try {
            await api.badges.remove(this.props.match.params.badgeId);
            this.setState({ loading: false });
            this.alertSuccess()
        } catch (error) {
            this.setState({ loading: false, error: error });
            this.alertError()
        }
    };


    fetchData = async () => {
        this.setState({ loading: true, error: null });

        try {
            const data = await api.badges.read(this.props.match.params.badgeId);
            this.setState({ loading: false, data: data });
        } catch (error) {
            this.setState({ loading: false, error: error });
        }
    };

    componentDidMount() {
        this.fetchData();
    }

    render() {
        if (this.state.loading) {
          return <PageLoading />;
        }
    
        if (this.state.error) {
          return <PageError error={this.state.error} />;
        }
    
        return (
          <BadgeDetails
            onCloseModal={this.handleCloseModal}
            onOpenModal={this.handleOpenModal}
            modalIsOpen={this.state.modalIsOpen}
            onDeleteBadge={this.handleDeleteBadge}
            badge={this.state.data}
          />
        );
      }
}

export default BadgeDetailsContainer;